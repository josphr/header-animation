import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import styled from 'styled-components'

const Header = styled.header`
    background:lightgray;
    height: 80px;
    color: white;
    position:fixed;
    width: 100%;
    transition: transform .5s ease-in;
    ${props => props.slideUp ?  'transform: translateY(-100px)' : 'transform: translateY(0)'} ;
`

const TextLayout = styled.div`
    padding:450px 50px;
`

class App extends Component {

    constructor(props){
        super(props)
        this.state = {
          newScrollPosition : 0,
          lastScrollPosition : 0,
          slideUp : false
        }
        
    }


    componentDidMount(){
      window.addEventListener('scroll',this.onScroll)        
    }

    onScroll = e => {
     const { newScrollPosition , lastScrollPosition } = this.state 

    this.setState({
      lastScrollPosition : window.scrollY
    })

    if (newScrollPosition < lastScrollPosition && 
       (lastScrollPosition > 280 && newScrollPosition)){
          this.setState({
            slideUp : true
          })
    }
    else if (newScrollPosition > lastScrollPosition){
        setTimeout(() => this.setState({
          slideUp : false
        }),500)
    }

    this.setState({
      newScrollPosition : lastScrollPosition
    })
  }


  render() {
    const { slideUp } = this.state    

    return (
      <div className="App">
          <Header slideUp={slideUp}>
            <img src={logo} className="App-logo" alt="logo" />
          </Header>
          <TextLayout>
            <p>Lorem ipsum dolor sit amet, mea an ubique consectetuer. Rebum etiam tacimates cum an, vel facer aeterno eu, cum eu laudem nusquam vulputate. Ne primis volumus suscipit mea. Eu quo impetus inciderint reformidans, quo erant mandamus in. Sadipscing dissentiunt an sea, solum harum facilis vel ne, sed in falli affert.
            Utinam platonem mei ea, platonem hendrerit his ne. Offendit maluisset consectetuer has id, tale habemus an his. Meis contentiones no vix. Ei oratio omittam iudicabit vel, no illum putant his.      
            Cu malis menandri platonem his, accumsan praesent prodesset mei ad. Possit nonumes accommodare eu mei, et labores praesent eos. Ad liber accusamus qui, eos ad delectus quaestio perpetua, nam ne soleat pertinacia. Bonorum persecuti at mei. Pro elit oporteat salutatus ne, elit fugit fabellas ad vis. Harum impetus eam ei, ea aperiri quaeque salutandi est, ad melius volumus his.         
            Esse wisi complectitur est ne, mel ad numquam fastidii signiferumque. Eu pro partem maluisset, ius fuisset qualisque et. Ea cum elit mandamus, atqui consequat pro ea. Splendide reformidans consectetuer et usu, ei mea autem partem deleniti, vix ferri aperiri quaerendum ei.         
            Cum falli consectetuer ad, et cum nominati corrumpit, dicant veritus officiis ut est. Eu mel gubergren inciderint. Eam labore omnesque perpetua in. Vivendo similique mea te. Sed id equidem convenire expetenda, vide mediocrem complectitur mel at.</p>
          </TextLayout>
      </div>
    );
  }
}

export default App;
